
# Search, be simple
A simple AngularJS component based application with the latest ES6 features, that uses Flickr API for searching of millions of images posted by users all across the world.

> This application is based on the NG6-starter template: https://github.com/gdi2290/NG6-starter
