import angular from 'angular';
import uiRouter from 'angular-ui-router';
import resultsComponent from './results.component';

let resultsModule = angular.module('results', [
  uiRouter
])

.component('results', resultsComponent)

.name;

export default resultsModule;
