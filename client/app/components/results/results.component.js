import template from './results.html';
import controller from './results.controller';
import './results.scss';

let resultsComponent = {
  bindings: {},
  template,
  controller
};

export default resultsComponent;
