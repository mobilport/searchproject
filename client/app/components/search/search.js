import angular from 'angular';
import uiRouter from 'angular-ui-router';
import searchComponent from './search.component';
import resultsComponent from '../results/results.component';
import { SearchService } from '../../services/search.service';

let searchModule = angular.module('search', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
	"ngInject";

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('search', {
			url: '/',
			component: 'searchContainer'
		})
		.state('results', {
			url: '/result/:keyword',
			component: 'resultsContainer',
			resolve: {
				init: ($state, $stateParams) => {
					if (!$stateParams.keyword) {
						$state.go('search');
					}
				},
				searchResults: (SearchService, $state, $stateParams) => {
					return SearchService.searchByKeyword($stateParams.keyword)
						.then((response) => {
							console.log(response)
							return response
						})
						.catch((error) => {
							console.log(error);
							$state.go('search')
						});
				}
			}
		});
})
.service('SearchService', SearchService)
.component('search', searchComponent)
.component('results', resultsComponent)

.name;

export default searchModule;
