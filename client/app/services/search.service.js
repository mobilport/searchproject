/**
 * Created by koppany.kiss on 2017. 12. 25..
 */

const API_KEY = '57f694132e4714c29a64c9af890b124e';
const BASE_URL = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${API_KEY}&format=json&nojsoncallback=1&extras=url_n,url_h,description`

class SearchService {
	constructor() {
	}

	searchByKeyword(keyword) {
		let request = new Request(`${BASE_URL}&text=${keyword}`, {
			method: 'GET'
		});

		return fetch(request).then(response => {
			return response.json().then(data => {
				if (response.ok) {
					return Promise.resolve(data);
				} else {
					return Promise.reject({status: response.status, data});
				}
			});
		});
	}
}

export {SearchService};

