import ResultsContainerModule from './resultsContainer';
import ResultsContainerController from './resultsContainer.controller';
import ResultsContainerComponent from './resultsContainer.component';
import ResultsContainerTemplate from './resultsContainer.html';

describe('ResultsContainer', () => {
  let $rootScope, makeController;

  beforeEach(window.module(ResultsContainerModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new ResultsContainerController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(ResultsContainerTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = ResultsContainerComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(ResultsContainerTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(ResultsContainerController);
    });
  });
});
