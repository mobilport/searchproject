import template from './resultsContainer.html';
import controller from './resultsContainer.controller';
import './resultsContainer.scss';

let resultsContainerComponent = {
	bindings: {
		searchResults: '<'
  },
	template,
	controller
};

export default resultsContainerComponent;
