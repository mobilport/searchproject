import angular from 'angular';
import uiRouter from 'angular-ui-router';
import resultsContainerComponent from './resultsContainer.component';

let resultsContainerModule = angular.module('resultsContainer', [
  uiRouter
])

.component('resultsContainer', resultsContainerComponent)

.name;

export default resultsContainerModule;
