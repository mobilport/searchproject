import template from './resultItem.html';
import controller from './resultItem.controller';
import './resultItem.scss';

let resultItemComponent = {
  bindings: {
  	photoIndex: '<',
  	photoThumbUrl: '<',
  	photoUrl: '<',
  	photoTitle: '<',
  	photoDescription: '<',
  },
  template,
  controller
};

export default resultItemComponent;
