import ResultItemModule from './resultItem';
import ResultItemController from './resultItem.controller';
import ResultItemComponent from './resultItem.component';
import ResultItemTemplate from './resultItem.html';

describe('ResultItem', () => {
  let $rootScope, makeController;

  beforeEach(window.module(ResultItemModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new ResultItemController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(ResultItemTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = ResultItemComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(ResultItemTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(ResultItemController);
    });
  });
});
