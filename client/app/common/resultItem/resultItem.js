import angular from 'angular';
import uiRouter from 'angular-ui-router';
import resultItemComponent from './resultItem.component';

let resultItemModule = angular.module('resultItem', [
  uiRouter
])

.component('resultItem', resultItemComponent)

.name;

export default resultItemModule;
