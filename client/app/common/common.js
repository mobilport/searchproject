import angular from 'angular';
import SearchContainer from './searchContainer/searchContainer';
import ResultsContainer from './resultsContainer/resultsContainer';
import ResultItem from './resultItem/resultItem';
import SearchInput from './searchInput/searchInput';

let commonModule = angular.module('app.common', [
	SearchContainer,
	ResultsContainer,
	ResultItem,
	SearchInput
])
  
.name;

export default commonModule;
