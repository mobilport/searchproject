import angular from 'angular';
import uiRouter from 'angular-ui-router';
import searchContainerComponent from './searchContainer.component';

let searchContainerModule = angular.module('searchContainer', [
  uiRouter
])

.component('searchContainer', searchContainerComponent)

.name;

export default searchContainerModule;
