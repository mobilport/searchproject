import SearchContainerModule from './searchContainer';
import SearchContainerController from './searchContainer.controller';
import SearchContainerComponent from './searchContainer.component';
import SearchContainerTemplate from './searchContainer.html';

describe('SearchContainer', () => {
  let $rootScope, makeController;

  beforeEach(window.module(SearchContainerModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new SearchContainerController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(SearchContainerTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = SearchContainerComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(SearchContainerTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(SearchContainerController);
    });
  });
});
