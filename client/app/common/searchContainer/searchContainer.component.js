import template from './searchContainer.html';
import controller from './searchContainer.controller';
import './searchContainer.scss';

let searchContainerComponent = {
  bindings: {},
  template,
  controller
};

export default searchContainerComponent;
