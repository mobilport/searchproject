/**
 * Created by koppany.kiss on 2017. 12. 28..
 */

const predefinedKeywords = [
	{
		name: "Animals",
		value: [
			{
				name: "Pets",
				value: [
					{
						name: "Guppy"
					},
					{
						name: "Parrot"
					},
					{
						name: "Goldfish"
					},
					{
						name: "Dog"
					},
					{
						name: "Cat"
					},
				]
			},
			{
				name: "Wild animals",
				value: [
					{
						name: "Tiger"
					},
					{
						name: "Ant"
					},
					{
						name: "Tetra"
					},
					{
						name: "Peafowl"
					},
					{
						name: "Mongoose"
					},
				]
			},
			{
				name: "Domestic animals",
				value: [
					{
						name: "Cow"
					},
					{
						name: "Pig"
					},
					{
						name: "Goat"
					},
					{
						name: "Horse"
					},
				]
			},
		]
	},
	{
		name: "Food",
		value: [
			{
				name: "Fast food",
				value: [
					{
						name: "Cheeseburger"
					},
					{
						name: "Hamburger"
					}
				]
			},
			{
				name: "Dessert",
				value: [
					{
						name: "Chocolate"
					},
					{
						name: "Cookie"
					},
					{
						name: "Cake"
					},
					{
						name: "Pie"
					}
				]
			}
		]
	},
	{
		name: "Vehicle",
		value: [
			{
				name: "Motorcycle",
				value: [
					{
						name: "Harley Davidson"
					}
				]
			},
			{
				name: "Car",
				value: [
					{
						name: "Lamborghini"
					},
					{
						name: "Ferrari"
					},
					{
						name: "Bugatti"
					},
					{
						name: "BMW"
					},
					{
						name: "Mercedes"
					}
				]
			}
		]
	},
	{
		name: "Movie",
		value: [
			{
				name: "Science fiction",
				value: [
					{
						name: "Sunshine"
					},
					{
						name: "Interstellar"
					},
					{
						name: "The Moon"
					},
					{
						name: "Oblivion"
					},
					{
						name: "Star Trek"
					},
					{
						name: "Star Wars"
					}
				]
			}
		]
	}
];

export default predefinedKeywords;
