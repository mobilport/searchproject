import SearchInputModule from './searchInput';
import SearchInputController from './searchInput.controller';
import SearchInputComponent from './searchInput.component';
import SearchInputTemplate from './searchInput.html';

describe('SearchInput', () => {
  let $rootScope, makeController;

  beforeEach(window.module(SearchInputModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new SearchInputController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(SearchInputTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = SearchInputComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(SearchInputTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(SearchInputController);
    });
  });
});
