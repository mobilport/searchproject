import template from './searchInput.html';
import controller from './searchInput.controller';
import './searchInput.scss';

let searchInputComponent = {
  bindings: {
  	middle: '@',
  	showPredefinedKeywords: '@'
  },
  template,
  controller
};

export default searchInputComponent;
