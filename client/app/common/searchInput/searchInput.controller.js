import predefinedKeywords from './predefinedKeywords';

class SearchInputController {
  constructor($state, $stateParams) {
	'ngInject';
	this.name = 'searchInput';
	this.$state = $state;
	this.$stateParams = $stateParams;
	this.predefinedKeywords = predefinedKeywords;
	console.log(this.predefinedKeywords);
  }

  $onInit() {
  	if (this.$stateParams.keyword) {
  		this.keyword = this.$stateParams.keyword;
	}
  }

	doSearch(keyword) {
  		if (keyword) {
			this.$state.go('results', {keyword: keyword})
		} else if (this.keyword) {
			this.$state.go('results', {keyword: this.keyword})
		}
	}
}

export default SearchInputController;
