import angular from 'angular';
import uiRouter from 'angular-ui-router';
import searchInputComponent from './searchInput.component';

let searchInputModule = angular.module('searchInput', [
  uiRouter
])

.component('searchInput', searchInputComponent)

.name;

export default searchInputModule;
